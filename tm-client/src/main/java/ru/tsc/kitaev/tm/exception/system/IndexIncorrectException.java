package ru.tsc.kitaev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    public IndexIncorrectException(@NotNull final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(@NotNull final String value) {
        super("Error. This value `" + value + "` is not number.");
    }

}
