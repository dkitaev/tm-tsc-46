package ru.tsc.kitaev.tm.exception.system;

import ru.tsc.kitaev.tm.constant.TerminalConst;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use ``" + TerminalConst.HELP + "`` for display list of terminal commands");
    }

}
