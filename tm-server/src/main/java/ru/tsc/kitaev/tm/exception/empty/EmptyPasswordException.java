package ru.tsc.kitaev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty.");
    }

    public EmptyPasswordException(@NotNull String value) {
        super("Error" + value + " Password is empty.");
    }

}
