package ru.tsc.kitaev.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository implements IUserRepository {

    protected EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull User user) {
        entityManager.merge(user);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM User")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager
                .createQuery("FROM User", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public User findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM User u WHERE u.id = :id", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public User findByIndex(@NotNull Integer index) {
        return entityManager
                .createQuery("FROM User", User.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM User u WHERE u.login = :login", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", email)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.remove(findByLogin(login));
    }

}
