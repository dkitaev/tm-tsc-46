package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getSessionSecret();

    @NotNull
    Integer getSessionIteration();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcSqlDialect();

    @NotNull
    String getJdbcNbm2ddlAuto();

    @NotNull
    String getJdbcShowSql();

    @NotNull
    String getHibernateCacheUseSecondLevelCache();

    @NotNull
    String getHibernateCacheProviderConfigurationFileResourcePath();

    @NotNull
    String getHibernateCacheRegionFactoryClass();

    @NotNull
    String getHibernateCacheUseQueryCache();

    @NotNull
    String getHibernateCacheUseMinimalPuts();

    @NotNull
    String getHibernateCacheHazelcastUseLiteMember();

    @NotNull
    String getHibernateCacheRegionPrefix();

}
