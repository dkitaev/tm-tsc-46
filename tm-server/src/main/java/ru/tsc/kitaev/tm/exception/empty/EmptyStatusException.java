package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error. Status is empty.");
    }

}
