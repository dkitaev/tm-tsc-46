package ru.tsc.kitaev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.model.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.model.UserRepository;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
        this.propertyService = propertyService;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findByIndex(@NotNull final Integer index) {
        if (index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findById(id);
            return user != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            userRepository.findByIndex(index);
            return true;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final List<User> users) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            for (User user : users) {
                userRepository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByLogin(login);
            return user != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByEmail(email);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByEmail(email);
            return user != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isLoginExists(login)) throw new LoginExistsException();
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        user.setEmail(email);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (isLoginExists(login)) throw new LoginExistsException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(role);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        setPassword(user, password);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(@Nullable final User user, @Nullable final String password) {
        if (user == null) throw new EntityNotFoundException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
