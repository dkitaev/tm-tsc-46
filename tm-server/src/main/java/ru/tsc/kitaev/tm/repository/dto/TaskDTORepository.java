package ru.tsc.kitaev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDTORepository implements ITaskDTORepository {

    protected EntityManager entityManager;

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final TaskDTO task) {
        entityManager.persist(task);
    }

    @Override
    public void update(@NotNull final TaskDTO task) {
        entityManager.merge(task);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO t WHERE t.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public @NotNull TaskDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findByIndex(userId, index));
    }

    @Override
    public @NotNull Integer getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public @NotNull TaskDTO findByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE t.userId = :userId AND t.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public @NotNull List<TaskDTO> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        entityManager
                .createQuery("UPDATE TaskDTO t SET t.projectId = :projectId WHERE t.userId = :userId AND t.id = :id")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("taskId", taskId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        entityManager
                .createQuery("UPDATE TaskDTO t SET t.projectId = NULL WHERE t.userId = :userId AND t.projectId = :projectId AND t.id = :id")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("taskId", taskId)
                .executeUpdate();
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
