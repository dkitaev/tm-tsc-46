package ru.tsc.kitaev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.model.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.model.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.model.IProjectTaskService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.model.ProjectRepository;
import ru.tsc.kitaev.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void bindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final Task task = taskRepository.findById(userId, taskId);
            @NotNull final Project project = projectRepository.findById(userId, projectId);
            task.setProject(project);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final Task task = taskRepository.findById(userId, taskId);
            task.setProject(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final String projectId = projectRepository.findByIndex(userId, index).getId();
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final String projectId = projectRepository.findByName(userId, name).getId();
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
