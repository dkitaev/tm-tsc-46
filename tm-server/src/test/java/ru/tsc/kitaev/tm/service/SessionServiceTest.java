package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kitaev.tm.component.Bootstrap;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.service.dto.SessionDTOService;

public class SessionServiceTest {

    @NotNull
    private final ISessionDTOService sessionService;

    @NotNull
    private SessionDTO session;


    public SessionServiceTest() {
        sessionService = new SessionDTOService(
                new ConnectionService(new PropertyService()), new LogService(), new Bootstrap()
        );
    }

    @Before
    public void before() {
        @NotNull final String userLogin = "userLogin";
        @NotNull final String userPassword = "userPassword";
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void openTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void closeTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    public void validateTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session);
    }

    @Test
    public void validateRoleTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session, Role.ADMIN);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

}
