package ru.tsc.kitaev.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

public class ProjectRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final IProjectDTORepository projectRepository;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProjectDescription";

    @NotNull
    private final String userId;

    public ProjectRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        projectRepository = sqlSession.getMapper(IProjectDTORepository.class);
        userRepository = sqlSession.getMapper(IUserDTORepository.class);
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        userRepository.add(user);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        sqlSession.commit();
    }

    @Before
    public void before() {
        projectRepository.add(project);
        sqlSession.commit();
    }

    @Test
    public void findProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(userId, 0));
        Assert.assertEquals(project, projectRepository.findByName(userId, projectName));
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.removeById(userId, projectId);
        sqlSession.commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectRepository.removeByIndex(userId, 0);
        sqlSession.commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.removeByName(userId, projectName);
        sqlSession.commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String newName = "newTestProject";
        @NotNull final String newDescription = "newTestProjectDescription";
        projectRepository.updateById(userId, projectId, newName, newDescription);
        sqlSession.commit();
        Assert.assertEquals(newName, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final String newName = "newTestProject";
        @NotNull final String newDescription = "newTestProjectDescription";
        projectRepository.updateByIndex(userId, 0, newName, newDescription);
        sqlSession.commit();
        Assert.assertEquals(newName, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.startById(userId, projectId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.startByIndex(userId, 0, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.startByName(userId, projectName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.finishById(userId, projectId, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.finishByIndex(userId, 0, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.finishByName(userId, projectName, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.changeStatusById(userId, projectId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.changeStatusByName(userId, projectName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() {
        projectRepository.clearByUserId(userId);
        userRepository.removeById(userId);
        sqlSession.commit();
        sqlSession.close();
    }

}
